import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule,Routes} from '@angular/router'
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { SelectionComponent } from './selection/selection.component';
import { BillingComponent } from './billing/billing.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { hostViewClassName } from '@angular/compiler';


const route:Routes=[
  {
    path:'',
    component:HomeComponent
  },
  {
    path:'selection',
    component:SelectionComponent
  },
  {
    path:'billing',
    component:BillingComponent
  },
    {path:'side',
    component:SidebarComponent
    }
    ]
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    SelectionComponent,
    BillingComponent,
    SidebarComponent,
  ],
  imports: [
    BrowserModule,RouterModule.forRoot(route)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
