
export class alldata { 
    //fruits array
    froots:Array<any> = [
    {
    name: 'APPLE',
    category: 'Fruits',
    mrp: 20,
}, {
    name: 'BANANA',
    category: 'Fruits',
    mrp: 6,
}, {
    name: 'ORANGE',
    category: 'Fruits',
    mrp: 10,
}, {
    name: 'GRAPES',
    category: 'Fruits',
    mrp: 120,
}, {
    name: 'CHERRY',
    category: 'Fruits',
    mrp: 180,
}, {
    name: 'COCONUT',
    category: 'Fruits',
    mrp: 90,
}, {
    name: 'DATE',
    category: 'Fruits',
    mrp: 200,
}, {
    name: 'CRANBERRY',
    category: 'Fruits',
    mrp: 220,
}, {
    name: 'MANGO',
    category: 'Fruits',
    mrp: 70,
}, {
    name: 'MELON',
    category: 'Fruits',
    mrp: 40,
}];
//vegetable array
veg:Array<any> = [{
    name: 'CABBAGE',
    category: 'Vegetables',
    mrp: 12,

}, {
    name: 'CAPSICUM',
    category: 'Vegetables',
    mrp: 12,

}, {
    name: 'CARROT',
    category: 'Vegetables',
    mrp: 12,

}, {
    name: 'POTATO',
    category: 'Vegetables',
    mrp: 12,

}, {
    name: 'BEANS',
    category: 'Vegetables',
    mrp: 12,

}, {
    name: 'LADY FINGER',
    category: 'Vegetables',
    mrp: 12,

}, {
    name: 'TOMATO',
    category: 'Vegetables',
    mrp: 12,

}, {
    name: 'EGG PLANT',
    category: 'Vegetables',
    mrp: 12,

}, {
    name: 'RED CHILLI',
    category: 'Vegetables',
    mrp: 12,

}, {
    name: 'GREEN CHILLI',
    category: 'Vegetables',
    mrp: 12,

}];
//drinks array
 drinks:Array<any> = [{
    name: 'COKE',
    category: 'Drinks',
    mrp: 12,

}, {
    name: 'FANTA',
    category: 'Drinks',
    mrp: 12,

}, {
    name: 'BANTA',
    category: 'Drinks',
    mrp: 12,

}, {
    name: 'JEERA SODA',
    category: 'Drinks',
    mrp: 12,

}, {
    name: 'LIMCA',
    category: 'Drinks',
    mrp: 12,

}, {
    name: 'SPRITE',
    category: 'Drinks',
    mrp: 12,

}, {
    name: 'THUMPSUP',
    category: 'Drinks',
    mrp: 12,

}, {
    name: 'PEPSI',
    category: 'Drinks',
    mrp: 12,

}, {
    name: 'LEMON SODA',
    category: 'Drinks',
    mrp: 12,

}, {
    name: 'DEW',
    category: 'Drinks',
    mrp: 12,

}];
//dairy products
milk:Array<any> = [{
    name: 'MILK',
    category: 'Dairy',
    mrp: 12,

}, {
    name: 'CURD',
    category: 'Dairy',
    mrp: 12,

}, {
    name: 'YOGURT',
    category: 'Dairy',
    mrp: 12,

}, {
    name: 'CHEESE',
    category: 'Dairy',
    mrp: 12,

}, {
    name: 'BUTTER',
    category: 'Dairy',
    mrp: 12,

}, {
    name: 'GHEE',
    category: 'Dairy',
    mrp: 12,

}, {
    name: 'CONDENSED MILK',
    category: 'Dairy',
    mrp: 12,

}, {
    name: 'CREAM',
    category: 'Dairy',
    mrp: 12,

}, {
    name: 'CHAAS',
    category: 'Dairy',
    mrp: 12,

}, {
    name: 'BASUNDI',
    category: 'Dairy',
    mrp: 12

}]
}
