import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  toggle:boolean=false

  constructor() {} 

    nav(){
      if(this.toggle==true){
        this.openNav()
      }
      else{
        this.closeNav()
        
      }

    }

  
  openNav() {
    document.getElementById("mySidenav").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
    this.toggle=false;
}

  closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("main").style.marginLeft= "0";
    this.toggle=true;
}
  title = "Welcome to GADGET HUB"
  ngOnInit() {
  }

}
